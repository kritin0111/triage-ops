# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/change/feature_flag_config_change'

RSpec.describe Triage::FeatureFlagConfigChange do
  describe '.file_patterns' do
    it 'matches dependency files' do
      files = %w[
        config/feature_flags/development/changelog_commits_limitation.yml
        config/feature_flags/development/exit_registration_verification.yml
        config/feature_flags/ops/authenticate_markdown_api.yml
        config/feature_flags/ops/database_async_index_creation.yml
      ]

      expect(files).to all(match(described_class.file_patterns))
    end
  end

  describe '.line_patterns' do
    it 'matches anything' do
      diffs = [
        '+ default_enabled: true',
        '+ default_enabled: false',
        '- default_enabled: true',
        '- default_enabled: false'
      ]

      expect(diffs).to all(match(described_class.line_patterns))
    end
  end
end
