# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/stage_definition'

RSpec.describe StageDefinition do
  let(:fake_group_name) { 'fake_group' }
  let(:fake_group_definition) do
    {
      'stage' => 'fake_stage',
      'backend_engineering_managers' => ['fake_be_em'],
      'frontend_engineering_managers' => ['fake_fe_em'],
      'product_managers' => ['fake_pm'],
      'backend_engineers' => ['fake_be'],
      'frontend_engineers' => ['fake_fe'],
      'product_designers' => ['fake_pd'],
      'label' => 'group::fake_group',
      'extra_labels' => ['extra-label'],
      'triage_ops_config' => {
        'default_labeling' => { 'groups' => [1], 'projects' => [2], 'unknown' => [3] }
      }
    }
  end

  let(:fake_stage_name) { 'fake_stage' }
  let(:fake_stage_definition) do
    {
      'label' => 'devops::fake_stage',
      'groups' => ['fake_group']
    }
  end

  subject(:stage) { described_class.new(fake_stage_name, fake_stage_definition) }

  before do
    stub_const("#{described_class}::STAGE_DATA", fake_stage_name => fake_stage_definition)
    stub_const("GroupDefinition::GROUP_DATA", fake_group_name => fake_group_definition)
  end

  describe '.find' do
    context 'when stage does not exist' do
      it 'returns nil' do
        expect(described_class.find('unknown_stage')).to be_nil
      end
    end

    context 'when stage exists' do
      it 'finds the stage by name' do
        expect(described_class.find('fake_stage').groups).to eq({ fake_group_name => fake_group_definition })
      end
    end
  end

  describe '#assignees' do
    context 'when fields is not passed' do
      it 'returns all default assignees' do
        expect(stage.assignees).to eq(%w[@fake_be_em @fake_fe_em @fake_pm])
      end
    end

    context 'when fields are passed' do
      it 'returns matching assignees' do
        expect(stage.assignees(%w[backend_engineering_managers frontend_engineering_managers])).to eq(%w[@fake_be_em @fake_fe_em])
      end
    end
  end

  describe '#labels' do
    it 'returns all labels' do
      expect(stage.labels).to eq([fake_stage_definition['label']])
    end
  end

  describe '#label' do
    it 'returns the stage label' do
      expect(stage.label).to eq(fake_stage_definition['label'])
    end
  end

  describe '#mentions' do
    context 'when fields are not passed' do
      it 'returns all default mentions' do
        expect(stage.mentions).to eq(%w[@fake_be @fake_fe @fake_pd])
      end
    end

    context 'when fields are passed' do
      it 'returns []' do
        expect(stage.mentions(%w[backend_engineers frontend_engineers])).to eq([])
      end
    end
  end

  describe '#to_h' do
    it 'returns a hash' do
      expect(stage.to_h.keys).to eq(%i[assignees labels groups mentions])
    end
  end
end
