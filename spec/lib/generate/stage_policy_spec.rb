# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/generate/option'
require_relative '../../../lib/generate/stage_policy'

RSpec.describe Generate::StagePolicy do
  let(:options) { Generate::Option.new(template: template_path, only: only, assign: nil) }
  let(:template_path) { 'path/to/template' }
  let(:only) { nil }
  let(:template_name) { File.basename(template_path) }
  let(:output_dir) { "#{described_class.destination}/#{template_name}" }
  let(:fake_stage) { 'plan' }
  let(:fake_stage_definition) { {} }

  before do
    expect(File)
      .to receive(:read)
      .with(template_path)
      .and_return(<<~ERB)
        <%= stage_name %>
        <%= stage_label_name %>
        <%= assignees %>
        <%= groups %>
      ERB

    expect(FileUtils)
      .to receive(:rm_r)

    expect(FileUtils)
      .to receive(:mkdir_p)
      .with(output_dir)

    stub_const('StageDefinition::STAGE_DATA', fake_stage => fake_stage_definition)
    stub_const('GroupDefinition::GROUP_DATA', 'fake_group' => { 'backend_engineering_managers' => ['fake_assignee'] })
  end

  describe '.run' do
    let(:fake_stage) { 'create' }

    shared_examples 'generates and writes policy files' do
      it 'generates' do
        expect(StageDefinition)
          .to receive(:find)
          .with(fake_stage)
          .and_return(StageDefinition.new(fake_stage, { 'label' => "devops::#{fake_stage}", 'groups' => ['fake_group'] }))

        expect(File)
          .to receive(:write)
          .with(
            "#{output_dir}/#{fake_stage}.yml",
            <<~MARKDOWN)
              #{fake_stage}
              devops::#{fake_stage}
              ["@fake_assignee"]
              {"fake_group"=>{"backend_engineering_managers"=>["fake_assignee"]}}
            MARKDOWN

        described_class.run(options)
      end
    end

    it_behaves_like 'generates and writes policy files'

    context 'when --only is provided with the group' do
      let(:only) { ['create'] }

      it_behaves_like 'generates and writes policy files'
    end

    context 'when --only is provided with another group' do
      let(:only) { ['verify'] }

      it 'does not generate anything' do
        expect(File)
          .not_to receive(:write)

        described_class.run(options)
      end
    end
  end
end
