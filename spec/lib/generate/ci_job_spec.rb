# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/generate/ci_job'

RSpec.describe Generate::CiJob do
  let(:options) { double(:options, template: template_path) }
  let(:template_path) { 'path/to/template' }
  let(:template_name) { File.basename(template_path) }
  let(:output_dir) { described_class.destination }
  let(:fake_group) { 'fake' }

  describe '.run' do
    it 'generates and writes CI file' do
      expect(File)
      .to receive(:read)
      .with(template_path)
      .and_return(<<~ERB)
        <% teams.each do |team| -%>
        <%= team.name %>: <%= team.policy_file %>
        <% end -%>
      ERB

      expect(FileUtils)
        .to receive(:mkdir_p)
        .with(output_dir)

      expect(Dir).to receive(:[]).and_return(["#{fake_group}.yml"])

      expect(File)
        .to receive(:write)
        .with(
          "#{output_dir}/#{template_name}.yml",
          <<~MARKDOWN)
            #{fake_group}: policies/generated/#{template_name}/#{fake_group}.yml
          MARKDOWN

      described_class.run(options)
    end
  end

  describe described_class::Group do
    describe '#default_labeling' do
      let(:default_labeling_spec) { { 'groups' => [1, 2], 'projects' => [3] } }

      context 'when group definition has a default labeling config' do
        let(:group) { described_class.new(:foo, { 'triage_ops_config' => { 'default_labeling' => default_labeling_spec } }, '') }

        it 'fetches the default_labeling config' do
          expect(group.default_labeling).to eq(default_labeling_spec)
        end
      end

      context 'when group definition does not have a default labeling config' do
        let(:group) { described_class.new(:foo, {}, '') }

        it 'returns {}' do
          expect(group.default_labeling).to eq({})
        end
      end
    end
  end
end
