# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/group_definition'

RSpec.describe GroupDefinition do
  let(:fake_group_name) { 'fake_group' }
  let(:fake_group_definition) do
    {
      'stage' => 'fake_stage',
      'backend_engineering_managers' => ['fake_be_em'],
      'frontend_engineering_managers' => ['fake_fe_em'],
      'product_managers' => ['fake_pm'],
      'backend_engineers' => ['fake_be'],
      'frontend_engineers' => ['fake_fe'],
      'product_designers' => ['fake_pd'],
      'label' => 'group::fake_group',
      'extra_labels' => ['extra-label'],
      'triage_ops_config' => {
        'default_labeling' => { 'groups' => [1], 'projects' => [2], 'unknown' => [3] }
      }
    }
  end

  subject(:group) { described_class.new(fake_group_name, fake_group_definition) }

  before do
    stub_const("StageDefinition::STAGE_DATA", 'fake_stage' => {})
    stub_const("#{described_class}::GROUP_DATA", fake_group_name => fake_group_definition)
  end

  describe '.find' do
    it 'finds the group by name' do
      group = described_class.find('fake_group')

      expect(group.stage.name).to eq(fake_group_definition['stage'])
    end
  end

  describe '.pm_for_team' do
    it 'returns pm for team' do
      expect(described_class.pm_for_team('fake_group')).to eq(['@fake_pm'])
    end
  end

  describe '.em_for_team' do
    context 'when scope is backend' do
      it 'returns backend em for team' do
        expect(described_class.em_for_team('fake_group')).to eq(['@fake_be_em'])
      end
    end

    context 'when scope is frontend' do
      it 'returns frontend em for team' do
        expect(described_class.em_for_team('fake_group', :frontend)).to eq(['@fake_fe_em'])
      end
    end
  end

  describe '#assignees' do
    context 'when fields is not passed' do
      it 'returns all default assignees' do
        expect(group.assignees).to eq(%w[@fake_be_em @fake_fe_em @fake_pm])
      end
    end

    context 'when fields are passed' do
      it 'returns matching assignees' do
        expect(group.assignees(%w[backend_engineering_managers frontend_engineering_managers])).to eq(%w[@fake_be_em @fake_fe_em])
      end
    end
  end

  describe '#labels' do
    it 'returns all labels' do
      expect(group.labels).to eq([fake_group_definition['label']] + fake_group_definition['extra_labels'])
    end
  end

  describe '#label' do
    it 'returns the group label' do
      expect(group.label).to eq(fake_group_definition['label'])
    end
  end

  describe '#extra_labels' do
    it 'returns all extra_labelss' do
      expect(group.extra_labels).to eq(fake_group_definition['extra_labels'])
    end
  end

  describe '#stage' do
    it 'returns the satge' do
      expect(group.stage).to be_a(StageDefinition)
      expect(group.stage.name).to eq(fake_group_definition['stage'])
    end
  end

  describe '#mentions' do
    context 'when fields are not passed' do
      it 'returns all default mentions' do
        expect(group.mentions).to eq(%w[@fake_be @fake_fe @fake_pd])
      end
    end

    context 'when fields are passed' do
      it 'returns []' do
        expect(group.mentions(%w[backend_engineers frontend_engineers])).to eq([])
      end
    end
  end

  describe '#default_labeling' do
    context 'when default_labeling config is not present' do
      before do
        fake_group_definition.delete('triage_ops_config')
      end

      it 'returns the default_labeling config' do
        expect(group.default_labeling).to eq({})
      end
    end

    context 'when default_labeling config is present' do
      it 'returns the default_labeling config' do
        expect(group.default_labeling).to eq({ 'groups' => [1], 'projects' => [2] })
      end
    end
  end

  describe '#to_h' do
    it 'returns a hash' do
      expect(group.to_h.keys).to eq(%i[assignees labels stage mentions default_labeling])
    end
  end
end
