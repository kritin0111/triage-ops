# frozen_string_literal: true

require_relative '../../lib/devops_labels'
require_relative '../triage/pipeline_failure/spec_duration_data'

module Triage
  CiJob = Struct.new(:instance, :project_id, :job_id, :job_name, keyword_init: true) do
    def retry
      api_client.job_retry(project_id, job_id).to_h
    end

    def trace
      @trace ||= (api_client.job_trace(project_id, job_id) || '')
    end

    def failed_rspec_test_metadata
      return [] unless rspec_job_artifact_to_json.any?

      rspec_job_artifact_to_json.select { |example| example["status"] == "failed" }.each do |example|
        labels_map = group_category_labels_for(example['feature_category'])

        if labels_map.present?
          example['product_group_label'] = labels_map['group_label']
          example['feature_category_label'] = labels_map['category_label']
        else
          example['product_group_label'] = DevopsLabels::MISSING_PRODUCT_GROUP_LABEL
          example['feature_category_label'] = DevopsLabels::MISSING_FEATURE_CATEGORY_LABEL
        end
      end
    end

    def spec_duration_reports
      return unless knapsack_expected_durations.any? && knapsack_actual_durations.any?

      knapsack_actual_durations.keys.map do |spec|
        Triage::PipelineFailure::SpecDurationData.new(
          spec_file: spec,
          expected_duration: knapsack_expected_durations[spec],
          actual_duration: knapsack_actual_durations[spec]
        )
      end
    end

    private

    def api_client
      @api_client ||= Triage.api_client(instance)
    end

    def rspec_result_artifact_file
      @failed_job_artifact ||=
        api_client.download_job_artifact_file(project_id, job_id, "rspec/rspec-#{job_id}.json")
    end

    def knapsack_expected_durations
      @knapsack_expected_durations ||=
        knapsack_artifact_to_json('knapsack/node_specs_expected_duration.json')
    end

    def knapsack_actual_durations
      @knapsack_actual_durations ||=
        knapsack_artifact_to_json("knapsack/#{job_name.gsub(%r{[ /]}, '_')}_report.json")
    end

    def rspec_job_artifact_to_json
      @rspec_job_artifact_to_json ||= begin
        JSON.parse(rspec_result_artifact_file.read)["examples"]
      rescue StandardError
        []
      end
    end

    def knapsack_artifact_to_json(file_name)
      report = api_client.download_job_artifact_file(project_id, job_id, file_name).read
      JSON.parse(report)
    rescue StandardError
      {}
    end

    def group_category_labels_for(feature_category)
      DevopsLabels.group_category_labels_per_category(feature_category)
    end
  end
end
