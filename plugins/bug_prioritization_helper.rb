# frozen_string_literal: true

require_relative '../lib/bug_prioritization_helper'

Gitlab::Triage::Resource::Context.include BugPrioritizationHelperContext
