# frozen_string_literal: true

require 'yaml'

require_relative './group_definition'
require_relative './team_member_helper'
require_relative './www_gitlab_com'

StageStruct = Struct.new(:name, :definition)

class StageDefinition < StageStruct
  STAGE_DATA = WwwGitLabCom.stages
  DEFAULT_ASSIGNEE_FIELDS = %w[
    product_managers
    engineering_managers
    fullstack_managers
    backend_engineering_managers
    frontend_engineering_managers
    software_engineers_in_test
    product_design_managers
  ].freeze
  MENTION_FIELDS = %w[
    backend_engineers
    frontend_engineers
    product_designers
  ].freeze
  ALL_ASSIGNEE_FIELDS = DEFAULT_ASSIGNEE_FIELDS + MENTION_FIELDS

  def self.find(name)
    new(name, STAGE_DATA.fetch(name)) if STAGE_DATA.key?(name)
  end

  def assignees(fields = nil)
    TeamMemberHelper.build_unique_mentions(assignables.values_at(*assignee_fields(fields)).flatten.compact)
  end

  def labels
    @labels ||= [label].compact
  end

  def label
    @label ||= definition['label']
  end

  def groups
    @groups ||= GroupDefinition::GROUP_DATA.slice(*definition['groups'])
  end

  def mentions(fields = nil)
    return [] if fields

    TeamMemberHelper.build_unique_mentions(assignables.values_at(*MENTION_FIELDS).compact)
  end

  def to_h(fields = nil)
    {
      assignees: assignees(fields),
      labels: labels,
      groups: groups,
      mentions: mentions(fields)
    }.compact
  end

  private

  def assignables
    @assignables ||= groups.each_value.reduce({}) do |acc, details|
      acc.merge(details.slice(*ALL_ASSIGNEE_FIELDS)) { |k, a, b| (a + b).uniq }
    end.compact
  end

  def assignee_fields(fields)
    if fields
      fields & DEFAULT_ASSIGNEE_FIELDS
    else
      DEFAULT_ASSIGNEE_FIELDS
    end
  end
end
