# frozen_string_literal: true

require 'erb'
require 'fileutils'

require_relative '../group_definition'

module Generate
  module GroupPolicy
    def self.run(options)
      template_name = File.basename(options.template)
      erb = ERB.new(File.read(options.template), trim_mode: '-')

      FileUtils.rm_rf("#{destination}/#{template_name}", secure: true)
      FileUtils.mkdir_p("#{destination}/#{template_name}")

      GroupDefinition::GROUP_DATA.each do |name, definition|
        next if options.only && !options.only.include?(name)
        next if definition.dig('triage_ops_config', 'ignore_templates')&.include?(options.template)

        group = GroupDefinition.find(name)
        raise "Group #{name} not found!" if group.nil?

        generated_template_path = "#{destination}/#{template_name}/#{name}.yml"
        puts "Generating #{generated_template_path}"

        File.write(
          generated_template_path,
          erb.result_with_hash(
            group_name: name,
            group_label_name: group.label,
            stage_label_name: group.stage&.label,
            extra_labels: group.extra_labels,
            default_labeling: group.default_labeling
          ).gsub(/REMOVE ME\n/, '')
        )
      end
    end

    def self.destination
      @destination ||=
        File.expand_path('generated', "#{__dir__}/../../policies")
    end
  end
end
