# frozen_string_literal: true

require 'yaml'
require 'erb'
require 'fileutils'

require_relative '../group_definition'
require_relative '../www_gitlab_com'

module Generate
  module CiJob
    Group = Struct.new(:name, :definition, :policy_file) do
      def default_labeling
        (definition.dig('triage_ops_config', 'default_labeling') || {}).slice('groups', 'projects')
      end
    end

    def self.run(options)
      template_name = File.basename(options.template)
      erb = ERB.new(File.read(options.template), trim_mode: '-')
      policy_base = "policies/generated/#{template_name}"

      FileUtils.rm_rf(File.join(destination, "#{template_name}.yml"), secure: true)
      FileUtils.mkdir_p(destination)

      policy_file_names = Dir["#{policy_base}/*.{yml,yaml}"]
        .map { |policy_file| File.basename(policy_file) }.sort

      teams = policy_file_names.map do |file_name|
        name = File.basename(file_name, '.*')

        Group.new(name, GroupDefinition::GROUP_DATA.fetch(name, {}), "#{policy_base}/#{file_name}")
      end

      if teams.any?
        generated_jobs_path = "#{destination}/#{template_name}.yml"
        puts "Generating #{generated_jobs_path}"
        File.write(
          generated_jobs_path,
          erb.result_with_hash(teams: teams).squeeze("\n")
        )
      end
    end

    def self.destination
      @destination ||=
        File.expand_path('generated', "#{__dir__}/../../.gitlab/ci")
    end
  end
end
