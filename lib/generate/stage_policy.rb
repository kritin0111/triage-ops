# frozen_string_literal: true

require 'erb'
require 'fileutils'

require_relative '../stage_definition'

module Generate
  module StagePolicy
    def self.run(options)
      template_name = File.basename(options.template)
      erb = ERB.new(File.read(options.template), trim_mode: '-')

      FileUtils.rm_rf("#{destination}/#{template_name}", secure: true)
      FileUtils.mkdir_p("#{destination}/#{template_name}")

      StageDefinition::STAGE_DATA.each do |name, definition|
        next if options.only && !options.only.include?(name)

        stage = StageDefinition.find(name)
        raise "Stage #{name} not found!" if stage.nil?

        generated_template_path = "#{destination}/#{template_name}/#{name}.yml"
        puts "Generating #{generated_template_path}"

        File.write(
          generated_template_path,
          erb.result_with_hash(
            stage_name: name,
            stage_label_name: stage.label,
            assignees: stage.assignees(options.assign),
            groups: stage.groups
          )
        )
      end
    end

    def self.destination
      @destination ||=
        File.expand_path('generated', "#{__dir__}/../../policies")
    end
  end
end
