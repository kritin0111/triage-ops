# frozen_string_literal: true

require 'yaml'

require_relative './stage_definition'
require_relative './team_member_helper'
require_relative './www_gitlab_com'

GroupStruct = Struct.new(:name, :definition)

class GroupDefinition < GroupStruct
  SPECIAL_GROUPS_DATA = YAML.safe_load(File.read(File.expand_path("#{__dir__}/../group-definition.yml")), aliases: true).freeze
  GROUP_DATA = WwwGitLabCom.groups.merge(SPECIAL_GROUPS_DATA)

  DEFAULT_ASSIGNEE_FIELDS = %w[
    product_managers
    engineering_managers
    fullstack_managers
    backend_engineering_managers
    frontend_engineering_managers
    software_engineers_in_test
    product_design_managers
  ].freeze
  MENTION_FIELDS = %w[
    backend_engineers
    frontend_engineers
    product_designers
  ].freeze

  def self.find(name)
    new(name, GROUP_DATA.fetch(name))
  end

  def self.pm_for_team(group)
    TeamMemberHelper.build_unique_mentions(GROUP_DATA.dig(group, 'product_managers'))
  end

  def self.em_for_team(group, scope = :backend)
    key = case scope
          when :backend
            'backend_engineering_managers'
          else
            'frontend_engineering_managers'
          end
    ems = GROUP_DATA.dig(group, key) || []
    ems = GROUP_DATA.dig(group, 'engineering_managers') if ems.empty?

    TeamMemberHelper.build_unique_mentions(ems)
  end

  def assignees(fields = nil)
    TeamMemberHelper.build_unique_mentions(definition.values_at(*assignee_fields(fields)))
  end

  def labels
    @labels ||= [label] + extra_labels
  end

  def label
    definition['label']
  end

  def extra_labels
    definition.fetch('extra_labels', [])
  end

  def stage
    @stage ||= StageDefinition.find(definition['stage'])
  end

  def mentions(fields = nil)
    return [] if fields

    mentions = definition.values_at(*MENTION_FIELDS).flatten.compact
    TeamMemberHelper.build_unique_mentions(mentions)
  end

  def default_labeling
    @default_labeling ||= (definition.dig('triage_ops_config', 'default_labeling') || {}).slice('groups', 'projects', 'exclude_projects')
  end

  def to_h(fields = nil)
    {
      assignees: assignees(fields),
      labels: labels,
      stage: stage,
      mentions: mentions(fields),
      default_labeling: default_labeling
    }.compact
  end

  private

  def assignee_fields(fields)
    if fields
      fields & DEFAULT_ASSIGNEE_FIELDS
    else
      DEFAULT_ASSIGNEE_FIELDS
    end
  end
end
